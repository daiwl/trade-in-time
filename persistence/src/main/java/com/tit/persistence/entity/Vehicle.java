package com.tit.persistence.entity;

import javax.persistence.*;

/**
 * Created by danil on 03.09.2016.
 */

@Entity
@Table(name = "vehicle")
public class Vehicle extends Identity<Long> {

    @Column
    private String login;

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }
}
