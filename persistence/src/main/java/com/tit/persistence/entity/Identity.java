package com.tit.persistence.entity;

import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;
import java.io.Serializable;

/**
 * Created by danil on 10.09.2016.
 */

@MappedSuperclass
abstract class Identity<T extends Serializable> {

    private T id;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    public T getId() {
        return id;
    }

    public void setId(T id) {
        this.id = id;
    }
}
