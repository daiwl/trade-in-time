package com.tit.persistence.conf;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.context.annotation.PropertySources;
import org.springframework.context.support.PropertySourcesPlaceholderConfigurer;

/**
 * Created by danil on 11.09.2016.
 */

@Configuration
@PropertySources({
        @PropertySource(value = "classpath:application.properties"),
        @PropertySource(value = "classpath:application_${profile.name}.properties", ignoreResourceNotFound = true)
})
public class PersistenceProperties {

    @Value("${ds.driver}")
    public String driver;

    @Value("${ds.url}")
    public String url;

    @Value("${ds.username}")
    public String username;

    @Value("${ds.password}")
    public String password;

    @Value("${db.init}")
    public String dbInit;

    @Value("${db.update}")
    public Boolean dbUpdate;

    @Bean
    public static PropertySourcesPlaceholderConfigurer propertyConfigInDev() {
        return new PropertySourcesPlaceholderConfigurer();
    }

}
