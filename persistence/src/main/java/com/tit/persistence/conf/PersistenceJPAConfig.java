package com.tit.persistence.conf;

import liquibase.exception.LiquibaseException;
import liquibase.integration.spring.SpringLiquibase;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.core.io.DefaultResourceLoader;
import org.springframework.dao.annotation.PersistenceExceptionTranslationPostProcessor;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.JpaVendorAdapter;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.vendor.Database;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import org.springframework.transaction.annotation.TransactionManagementConfigurer;

import javax.persistence.EntityManagerFactory;
import javax.sql.DataSource;
import java.util.Properties;

/**
 * Created by danil on 03.09.2016.
 */

@Configuration
@EnableTransactionManagement
@EnableJpaRepositories(basePackages = {
        "com.tit.persistence.dao"
})
@Import({PersistenceProperties.class})
@ComponentScan(basePackages = {"com.tit.persistence"})
public class PersistenceJPAConfig implements TransactionManagementConfigurer {
    private static final String LIQUIBASE_CHANGELOG_FILE = "classpath:liquibase/changes/changelog-master.xml";
    private final PersistenceProperties persistenceProperties;

    @Autowired
    public PersistenceJPAConfig(PersistenceProperties persistenceProperties) {
        this.persistenceProperties = persistenceProperties;
    }

    @Bean(name = "dataSource")
    public DriverManagerDataSource dataSource() {
        DriverManagerDataSource driverManagerDataSource = new DriverManagerDataSource();
        driverManagerDataSource.setDriverClassName(persistenceProperties.driver);
        driverManagerDataSource.setUrl(persistenceProperties.url);
        driverManagerDataSource.setUsername(persistenceProperties.username);
        driverManagerDataSource.setPassword(persistenceProperties.password);
        return driverManagerDataSource;
    }

    @Bean(name = "liquibase")
    public SpringLiquibase liquibase(DataSource dataSource) throws LiquibaseException {
        SpringLiquibase liquibase = new SpringLiquibase();
        liquibase.setDataSource(dataSource);
        liquibase.setChangeLog(LIQUIBASE_CHANGELOG_FILE);
        liquibase.setDropFirst(false);
        liquibase.setShouldRun(persistenceProperties.dbUpdate);
        liquibase.setResourceLoader(new DefaultResourceLoader());
        return liquibase;
    }

    @Override
    public PlatformTransactionManager annotationDrivenTransactionManager() {
        return new JpaTransactionManager(entityManagerFactory());
    }

    @Bean
    public PersistenceExceptionTranslationPostProcessor exceptionTranslation() {
        return new PersistenceExceptionTranslationPostProcessor();
    }

    @Bean(name = "entityManagerFactory")
    public EntityManagerFactory entityManagerFactory() {
        LocalContainerEntityManagerFactoryBean emf = new LocalContainerEntityManagerFactoryBean();
        emf.setDataSource(dataSource());
        emf.setPackagesToScan("com.tit.persistence.entity");
        emf.setJpaVendorAdapter(jpaVendorAdapter());
        emf.setJpaProperties(getJpaProperties());
        emf.afterPropertiesSet();
        return emf.getObject();
    }

    private JpaVendorAdapter jpaVendorAdapter() {
        HibernateJpaVendorAdapter hibernateJpaVendorAdapter = new HibernateJpaVendorAdapter();
        hibernateJpaVendorAdapter.setShowSql(false);
        hibernateJpaVendorAdapter.setGenerateDdl(true);
        hibernateJpaVendorAdapter.setDatabase(Database.POSTGRESQL);
        return hibernateJpaVendorAdapter;
    }

    private Properties getJpaProperties() {
        Properties properties = new Properties();
        properties.put(org.hibernate.cfg.Environment.DIALECT, "org.hibernate.dialect.PostgreSQLDialect");
        properties.put(org.hibernate.cfg.Environment.HBM2DDL_AUTO, persistenceProperties.dbInit);
        return properties;
    }

}
