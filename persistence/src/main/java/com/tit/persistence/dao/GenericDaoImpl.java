package com.tit.persistence.dao;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 * Created by danil on 03.09.2016.
 */

@Repository(value = "genericDao")
public class GenericDaoImpl implements GenericDao {

    @PersistenceContext
    private EntityManager em;

    public EntityManager getEm() {
        return em;
    }

    @Override
    @Transactional(propagation = Propagation.MANDATORY)
    public <T> void update(T entity) {
        em.persist(entity);
    }
}
