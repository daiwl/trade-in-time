package com.tit.persistence.dao;

import javax.persistence.EntityManager;

/**
 * Created by danil on 03.09.2016.
 */
public interface GenericDao {
    <T> void update(T entity);

    EntityManager getEm();
}
