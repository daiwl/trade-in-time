package com.tit.core.session;

import org.springframework.security.core.context.SecurityContextHolder;

/**
 * Created by danil on 11.09.2016.
 */
public final class UserSession {

    public static AuthUser getUser() {
        return (AuthUser) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
    }
}
