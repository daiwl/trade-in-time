package com.tit.core.session;

import org.springframework.security.core.GrantedAuthority;

import java.util.Collection;

/**
 * Created by danil on 06.09.2016.
 */
public class AuthUser extends org.springframework.security.core.userdetails.User {
    public AuthUser(String username, String password, Collection<? extends GrantedAuthority> authorities) {
        super(username, password, authorities);
    }
}
