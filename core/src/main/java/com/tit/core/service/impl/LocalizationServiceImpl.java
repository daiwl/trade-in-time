package com.tit.core.service.impl;

import com.tit.core.service.LocalizationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.MessageSourceResolvable;
import org.springframework.context.NoSuchMessageException;
import org.springframework.context.annotation.Scope;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Locale;

/**
 * Created by danil on 20.09.2016.
 */

@Service("localizationService")
@Scope("prototype")
@Transactional(readOnly = true)
public class LocalizationServiceImpl extends AbstractService implements LocalizationService {

    private final MessageSource messageSource;

    @Autowired
    public LocalizationServiceImpl(MessageSource messageSource) {
        this.messageSource = messageSource;
    }

    @Override
    public String translate(String key, Object... params) {
        Locale locale = LocaleContextHolder.getLocale();
        try {
            return messageSource.getMessage(key, params, locale);
        } catch (NoSuchMessageException e) {
            return key;
        }
    }

    @Override
    public String translate(MessageSourceResolvable key) {
        Locale locale = LocaleContextHolder.getLocale();
        try {
            return messageSource.getMessage(key, locale);
        } catch (NoSuchMessageException e) {
            return key.getDefaultMessage();
        }
    }
}
