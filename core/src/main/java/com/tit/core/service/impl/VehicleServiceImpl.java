package com.tit.core.service.impl;

import com.tit.core.record.VehicleRecord;
import com.tit.core.service.VehicleService;
import com.tit.persistence.entity.Vehicle;
import com.tit.persistence.dao.VehicleDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

/**
 * Created by danil on 03.09.2016.
 */

@Service("vehicleService")
@Scope("prototype")
@Transactional(readOnly = true)
public class VehicleServiceImpl extends AbstractService implements VehicleService {

    private final VehicleDao dao;

    @Autowired
    public VehicleServiceImpl(VehicleDao dao) {
        this.dao = dao;
    }

    @Override
    @Transactional(propagation = Propagation.REQUIRES_NEW)
    public Vehicle update(VehicleRecord record) {
        Vehicle vehicle = new Vehicle();
        vehicle.setLogin("adasdas");
        dao.update(vehicle);
        return vehicle;
    }

    @Override
    @Secured("ROLE_USER")
    public void check() {
        System.out.print(true);
    }
}
