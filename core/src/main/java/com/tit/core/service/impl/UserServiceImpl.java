package com.tit.core.service.impl;

import com.tit.core.session.AuthUser;
import com.tit.core.exception.LoginExistsException;
import com.tit.core.record.UserRecord;
import com.tit.core.service.UserService;
import com.tit.persistence.entity.Permission;
import com.tit.persistence.entity.Role;
import com.tit.persistence.entity.User;
import com.tit.persistence.dao.UserDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;
import java.util.stream.Collectors;

/**
 * Created by danil on 06.09.2016.
 */

@Service("userService")
@Scope("prototype")
@Transactional(propagation = Propagation.REQUIRED)
public class UserServiceImpl extends AbstractService implements UserService {

    private final UserDao userDao;

    private final PasswordEncoder passwordEncoder;

    @Autowired
    public UserServiceImpl(UserDao userDao, PasswordEncoder passwordEncoder) {
        this.userDao = userDao;
        this.passwordEncoder = passwordEncoder;
    }

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        User user = userDao.findByLogin(username);
        if (user == null) {
            throw new UsernameNotFoundException(username);
        }
        return new AuthUser(
                user.getLogin(),
                user.getPassword(),
                getAuthorities(user.getRoles()));
    }

    @Override
    @Transactional(propagation = Propagation.REQUIRES_NEW)
    public User update(UserRecord record) throws LoginExistsException {
        if (userDao.isUserWithLogin(record.getLogin())) {
            throw new LoginExistsException(record.getLogin());
        }

        User user = new User();
        user.setLogin(record.getLogin());
        user.setPassword(passwordEncoder.encode(record.getPassword()));

        Set<Role> roles = new HashSet<>();
        Role role = new Role();
        role.setName("Role-1");

        Set<Permission> permissions = new HashSet<>();
        Permission p1 = new Permission();
        p1.setName("Permission-1");
        permissions.add(p1);
        role.setPermissions(permissions);
        roles.add(role);
        user.setRoles(roles);

        userDao.update(user);
        return user;
    }

    private Set<? extends GrantedAuthority> getAuthorities(Set<Role> roles) {
        if (roles == null) {
            return Collections.emptySet();
        }
        return getGrantedAuthorities(getPermission(roles));
    }

    private Set<String> getPermission(Set<Role> roles) {
        Set<String> permission = new HashSet<>();
        List<Permission> collection = new ArrayList<>();
        for (Role role : roles) {
            collection.addAll(role.getPermissions());
        }
        permission.addAll(collection.stream().map(Permission::getName).collect(Collectors.toList()));
        return permission;
    }

    private Set<GrantedAuthority> getGrantedAuthorities(Set<String> privileges) {
        return privileges.stream().map(SimpleGrantedAuthority::new).collect(Collectors.toSet());
    }

}
