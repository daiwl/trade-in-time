package com.tit.core.conf;

import com.tit.persistence.conf.PersistenceJPAConfig;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

/**
 * Created by danil on 29.08.2016.
 */

@Configuration
@Import({CoreCompConfig.class, PersistenceJPAConfig.class})
public class CoreConfig {

}
