package com.tit.core.conf;

import com.tit.core.session.UserAuthenticationProvider;
import com.tit.core.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.access.expression.method.MethodSecurityExpressionHandler;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.method.configuration.GlobalMethodSecurityConfiguration;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

/**
 * Created by danil on 13.09.2016.
 */

@Configuration
@EnableGlobalMethodSecurity(securedEnabled = true)
public class CoreSecurityConfig extends GlobalMethodSecurityConfiguration {

    @Override
    protected MethodSecurityExpressionHandler createExpressionHandler() {
        return super.createExpressionHandler();
    }

    @Autowired
    public void registerGlobalAuthentication(
            AuthenticationProvider authenticationProvider,
            AuthenticationManagerBuilder auth,
            UserService userDetailsService,
            PasswordEncoder passwordEncoder) throws Exception {
        auth
                .authenticationProvider(authenticationProvider)
                .userDetailsService(userDetailsService)
                .passwordEncoder(passwordEncoder);
    }

    @Bean(name = "userAuthenticationProvider")
    public AuthenticationProvider userAuthenticationProvider(UserService userService, PasswordEncoder passwordEncoder) {
        return new UserAuthenticationProvider(userService, passwordEncoder);
    }

    @Bean(name = "passwordEncoder")
    public PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }
}