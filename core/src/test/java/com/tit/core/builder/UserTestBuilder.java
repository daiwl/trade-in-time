package com.tit.core.builder;

import com.tit.core.record.UserRecord;
import com.tit.persistence.entity.Permission;
import com.tit.persistence.entity.Role;
import com.tit.persistence.entity.User;

import java.util.HashSet;
import java.util.Set;

/**
 * Created by danil on 18.09.2016.
 */
public class UserTestBuilder {

    public static User userOne() {
        return UserBuilder.user()
                .login("one")
                .get();
    }

    public static UserRecord userOneRecord() {
        UserRecord record = new UserRecord();
        record.setLogin("one");
        return record;
    }

    public static User testUser() {
        return UserBuilder.user()
                .login("test")
                .password("test")
                .role("role-1")
                .permission("permission-1").add()
                .permission("permission-2").add()
                .permission("permission-3").add()
                .add()
                .get();
    }

    private static class UserBuilder {
        User user;

        UserBuilder() {
            this.user = new User();
        }

        static UserBuilder user() {
            return new UserBuilder();
        }

        UserBuilder login(String login) {
            this.user.setLogin(login);
            return this;
        }

        UserBuilder password(String password) {
            this.user.setPassword(password);
            return this;
        }

        UserRoleBuilder role(String name) {
            return new UserRoleBuilder(name, this);
        }

        User get() {
            return this.user;
        }

        class UserRoleBuilder {
            private Role role;
            private UserBuilder userBuilder;

            UserRoleBuilder(String name, UserBuilder userBuilder) {
                this.userBuilder = userBuilder;
                this.role = new Role();
                this.role.setName(name);
            }

            PermissionBuilder permission(String name) {
                return new PermissionBuilder(name, this);
            }

            UserBuilder add() {
                Set<Role> roles = user.getRoles();
                if (roles == null) {
                    roles = new HashSet<>();
                    user.setRoles(roles);
                }
                roles.add(this.role);
                return this.userBuilder;
            }

            class PermissionBuilder {
                private Permission permission;
                private UserRoleBuilder roleBuilder;

                PermissionBuilder(String name, UserRoleBuilder roleBuilder) {
                    this.roleBuilder = roleBuilder;
                    this.permission = new Permission();
                    this.permission.setName(name);
                }

                UserRoleBuilder add() {
                    Set<Permission> permissions = role.getPermissions();
                    if (permissions == null) {
                        permissions = new HashSet<>();
                        role.setPermissions(permissions);
                    }
                    permissions.add(this.permission);
                    return this.roleBuilder;
                }
            }
        }
    }
}
