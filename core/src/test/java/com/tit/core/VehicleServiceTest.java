package com.tit.core;

import com.tit.core.session.AuthUser;
import com.tit.core.session.UserSession;
import com.tit.core.service.VehicleService;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.security.test.context.support.WithUserDetails;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

/**
 * Created by danil on 07.09.2016.
 */

public class VehicleServiceTest extends AbstractServiceTest {

//    @InjectMocks
//    @Autowired
//    private UserService userService;

//    @Mock
//    @Autowired
//    private VehicleService vehicleService;

    //    @InjectMocks
//    @Autowired
    @Mock
    private VehicleService vehicleService;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
//    @WithMockUser(username="test", roles={"USER"})
    @WithUserDetails(value = "test", userDetailsServiceBeanName = "userService")
    public void checkProtected() {
        AuthUser user = UserSession.getUser();
        assertNotNull(user);
        assertEquals(user.getAuthorities().size(), 3);
    }
}
