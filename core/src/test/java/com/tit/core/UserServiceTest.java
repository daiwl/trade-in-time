package com.tit.core;

import com.tit.core.builder.UserTestBuilder;
import com.tit.core.exception.LoginExistsException;
import com.tit.core.service.UserService;
import com.tit.persistence.dao.UserDao;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import static org.mockito.Mockito.when;

/**
 * Created by danil on 18.09.2016.
 */
public class UserServiceTest extends AbstractServiceTest {

    @Autowired
    UserService userService;

    @Autowired
    UserDao userDao;

    @Test(expected = LoginExistsException.class)
    public void registryUserExist() throws LoginExistsException {
        when(userDao.isUserWithLogin("one")).thenReturn(true);
        userService.update(UserTestBuilder.userOneRecord());
    }
}
