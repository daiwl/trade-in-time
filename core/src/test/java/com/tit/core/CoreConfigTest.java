package com.tit.core;

import com.tit.core.builder.UserTestBuilder;
import com.tit.core.conf.CoreCompConfig;
import com.tit.persistence.dao.UserDao;
import com.tit.persistence.dao.UserDaoImpl;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

import javax.persistence.EntityManagerFactory;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

/**
 * Created by danil on 10.09.2016.
 */

@Configuration
@Import({CoreCompConfig.class})
class CoreConfigTest {

    @Bean(name = "entityManagerFactory")
    public EntityManagerFactory entityManagerFactory() {
        return mock(EntityManagerFactory.class);
    }

    @Bean
    public UserDao userDao() {
        UserDao mock = mock(UserDaoImpl.class);
        when(mock.findByLogin("test")).thenReturn(UserTestBuilder.testUser());
        return mock;
    }

}
