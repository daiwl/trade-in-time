package com.tit.core.rs.helper;

import com.tit.core.rs.form.ErrorForm;
import com.tit.core.rs.form.ResponseForm;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

/**
 * Created by danil on 19.09.2016.
 */

public class ResponseBuilder {
    private ResponseForm<Object> responseForm;

    private ResponseBuilder(ResponseForm<Object> responseForm) {
        this.responseForm = responseForm;
    }

    public static ResponseBuilder status(HttpStatus status) {
        return new ResponseBuilder(new ResponseForm<>(status));
    }

    public ResponseBuilder row(Object row) {
        this.responseForm.getRows().add(row);
        return this;
    }

    public ResponseBuilder error(String path, String message) {
        this.responseForm.getErrors().add(new ErrorForm(path, message));
        return this;
    }

    public ResponseBuilder error(String message) {
        this.responseForm.getErrors().add(new ErrorForm(message));
        return this;
    }

    public ResponseEntity get() {
        return ResponseEntity.status(responseForm.getStatus()).body(responseForm);
    }
}
