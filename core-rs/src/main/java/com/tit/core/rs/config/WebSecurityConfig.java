package com.tit.core.rs.config;

import com.tit.core.rs.handler.UserAuthenticationFailureHandler;
import com.tit.core.rs.handler.UserAuthenticationSuccessHandler;
import com.tit.core.rs.handler.UserLogoutHandler;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.web.authentication.logout.SecurityContextLogoutHandler;

/**
 * Created by danil on 11.09.2016.
 */

@Configuration
@EnableWebSecurity
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {

    @Override
    protected void configure(HttpSecurity http) throws Exception {

        http.sessionManagement()
                .sessionFixation().migrateSession();
//                .expiredUrl("/sessionExpired.html")
//                .invalidSessionUrl("/invalidSession.html");

        http.authorizeRequests()
                .antMatchers("/user/registry").permitAll()
                .antMatchers("/resources/**").permitAll()
                .anyRequest().authenticated()
                .and()
                .exceptionHandling().accessDeniedPage("/403")
                .and()
                .httpBasic()
                .and()
                .csrf().disable();

        http.formLogin()
                .loginPage("/auth/login").permitAll()
                .defaultSuccessUrl("/user/profile", true).permitAll()
                .usernameParameter("username").passwordParameter("password")
                .successHandler(loginSuccessHandler())
                .failureHandler(loginFailureHandler());

        http.logout()
                .logoutUrl("/auth/logout").permitAll()
                .logoutSuccessUrl("/auth/login?logout").permitAll()
                .logoutSuccessHandler(logoutHandler())
                .addLogoutHandler(logoutHandler())
                .addLogoutHandler(new SecurityContextLogoutHandler())
                .invalidateHttpSession(true)
                .clearAuthentication(true)
                .deleteCookies("JSESSIONID");

//        http.rememberMe()
//                .userDetailsService(userDetailsService)
//                .tokenRepository(persistentTokenRepository)
//                .rememberMeCookieName("REMEMBER_ME")
//                .rememberMeParameter("remember_me")
//                .tokenValiditySeconds(1209600)
//                .useSecureCookie(false)
//                .key(rememberMeKey);

//        AuthorityUtils

//        http.authorizeRequests()
//                .anyRequest().authenticated()
//                .and()
//                .exceptionHandling()
//                .accessDeniedPage("/403");


//        .antMatchers("/resources/**", "/signup", "/about").permitAll()
//                .antMatchers("/admin/**").hasRole("ADMIN")
//                .antMatchers("/db/**").access("hasRole('ADMIN') and hasRole('DBA')")

//        http.csrf().disable()
//                .authorizeRequests()
//                .antMatchers("/res/**").permitAll()
//                .anyRequest().authenticated()
//                .and()
//                .formLogin()
//                .loginPage("/loginDatabase.html")
//                .permitAll();

//        http
//                .authorizeRequests()
//                .antMatchers("/", "/homepage/**").permitAll()
//                .antMatchers("/admin/**").access("hasRole('ROLE_ADMIN')")
//                .antMatchers("/db/**").access("hasRole('ROLE_ADMIN') and hasRole('ROLE_DBA')")
//                .and().formLogin().loginPage("/login")
//                .usernameParameter("username").passwordParameter("password")
//                .and().exceptionHandling().accessDeniedPage("/Access_Denied");
    }

    @Bean(name = "loginSuccessHandler")
    public UserAuthenticationSuccessHandler loginSuccessHandler() {
        return new UserAuthenticationSuccessHandler();
    }

    @Bean(name = "loginFailureHandler")
    public UserAuthenticationFailureHandler loginFailureHandler() {
        return new UserAuthenticationFailureHandler();
    }

    @Bean(name = "logoutHandler")
    public UserLogoutHandler logoutHandler() {
        return new UserLogoutHandler();
    }

}