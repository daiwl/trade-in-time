package com.tit.core.rs.handler;

import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.authentication.SimpleUrlAuthenticationFailureHandler;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Created by danil on 18.09.2016.
 */
public class UserAuthenticationFailureHandler extends SimpleUrlAuthenticationFailureHandler {

    public UserAuthenticationFailureHandler() {
        super("/auth/login?error");
    }

    @Override
    public void onAuthenticationFailure(
            HttpServletRequest request,
            HttpServletResponse response,
            AuthenticationException exception) throws IOException, ServletException {
        response.sendError(HttpServletResponse.SC_UNAUTHORIZED, "Bad credentials");
        super.onAuthenticationFailure(request, response, exception);
    }
}