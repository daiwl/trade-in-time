package com.tit.core.rs.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Created by danil on 17.09.2016.
 */
abstract class AbstractController {
    final Logger logger = LoggerFactory.getLogger(getClass());

}
