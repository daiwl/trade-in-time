package com.tit.core.rs.controller;

import com.tit.core.session.AuthUser;
import com.tit.core.exception.LoginExistsException;
import com.tit.core.record.UserRecord;
import com.tit.core.service.UserService;
import com.tit.core.rs.exception.UnprocessableEntityException;
import com.tit.core.rs.helper.ResponseBuilder;
import com.tit.persistence.entity.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.validation.Valid;
import java.util.UUID;

/**
 * Created by danil on 11.09.2016.
 */

@Controller
@RequestMapping("/user")
public class UserController extends AbstractController {

    private final UserService userService;

    @Autowired
    public UserController(UserService userService) {
        this.userService = userService;
    }

    @RequestMapping(value = "/profile", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @ResponseBody
    public AuthProfile profile() {
        AuthUser token = (AuthUser) SecurityContextHolder.getContext().getAuthentication().getDetails();
        return new AuthProfile(token);
    }

    @RequestMapping(value = "/registry",
            method = RequestMethod.POST,
            consumes = MediaType.APPLICATION_JSON_UTF8_VALUE,
            produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity registration(@Valid @RequestBody UserRecord record,
                                       BindingResult result) throws MethodArgumentNotValidException, UnprocessableEntityException {
        if (result.hasErrors()) {
            throw new MethodArgumentNotValidException(null, result);
        }
        try {
            User user = userService.update(record);
            return ResponseBuilder.status(HttpStatus.CREATED).row(user).get();
        } catch (LoginExistsException e) {
            throw new UnprocessableEntityException("user.registry.login.exists", e.getLogin());
        }
    }

    private class AuthProfile {
        String username;
        String token;

        AuthProfile(AuthUser token) {
            this.username = token.getUsername();
            this.token = UUID.randomUUID().toString();
        }
    }

}
