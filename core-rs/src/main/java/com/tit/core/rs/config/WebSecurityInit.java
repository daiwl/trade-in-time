package com.tit.core.rs.config;

import org.springframework.security.web.context.AbstractSecurityWebApplicationInitializer;

/**
 * Created by danil on 11.09.2016.
 */
public class WebSecurityInit extends AbstractSecurityWebApplicationInitializer {
}