package com.tit.core.rs.exception;

import org.springframework.http.HttpStatus;

/**
 * Created by danil on 19.09.2016.
 */
public class UnprocessableEntityException extends ResponseStatusException {
    private Object[] params;

    public UnprocessableEntityException(String message, Object... params) {
        super(HttpStatus.UNPROCESSABLE_ENTITY, message);
        this.params = params;
    }

    public Object[] getParams() {
        return params;
    }

}
