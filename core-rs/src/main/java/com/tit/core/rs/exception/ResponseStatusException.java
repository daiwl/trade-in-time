package com.tit.core.rs.exception;

import org.springframework.http.HttpStatus;

/**
 * Created by danil on 19.09.2016.
 */
public class ResponseStatusException extends Exception {
    private HttpStatus status;

    ResponseStatusException(HttpStatus status, String message) {
        super(message);
        this.status = status;
    }

    public HttpStatus getStatus() {
        return status;
    }
}
