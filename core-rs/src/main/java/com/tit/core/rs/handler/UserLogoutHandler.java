package com.tit.core.rs.handler;

import org.springframework.security.core.Authentication;
import org.springframework.security.web.authentication.logout.LogoutHandler;
import org.springframework.security.web.authentication.logout.LogoutSuccessHandler;
import org.springframework.stereotype.Component;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Created by danil on 05.09.2016.
 */

@Component
public class UserLogoutHandler implements LogoutSuccessHandler, LogoutHandler {

    @Override
    public void onLogoutSuccess(HttpServletRequest httpServletRequest, HttpServletResponse response, Authentication authentication) throws IOException, ServletException {
        response.setStatus(HttpServletResponse.SC_OK);
    }

    @Override
    public void logout(HttpServletRequest httpServletRequest, HttpServletResponse response, Authentication authentication) {
//        response.setStatus(HttpServletResponse.SC_OK);
    }
}
