package com.tit.core.rs.form;

/**
 * Created by danil on 17.09.2016.
 */
public class ErrorForm {
    private int code = 0;
    private String path;
    private String message;

    public ErrorForm(int code, String path, String message) {
        this.code = code;
        this.path = path;
        this.message = message;
    }

    public ErrorForm(String path, String message) {
        this(0, path, message);
    }

    public ErrorForm(String message) {
        this(null, message);
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
