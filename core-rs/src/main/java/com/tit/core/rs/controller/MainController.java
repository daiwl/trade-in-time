package com.tit.core.rs.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.security.Principal;


/**
 * Created by danil on 29.08.2016.
 */

@Controller
public class MainController extends AbstractController {

//    @RequestMapping(value = "/", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
//    @ResponseBody
//    public AuthProfile web() {
//        AuthUser token = (AuthUser) SecurityContextHolder.getContext().getAuthentication().getDetails();
//        return new AuthProfile(token);
//    }

    @RequestMapping(value = "/403", method = RequestMethod.GET)
    public String accessDenied(ModelMap model, Principal principal) {
        String username = principal.getName();
        model.addAttribute("message", "Sorry " + username + " You don't have privileges to view this page!!!");
        return "errors/403";
    }

}
