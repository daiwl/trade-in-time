package com.tit.core.rs.handler;

import com.tit.core.service.LocalizationService;
import com.tit.core.rs.exception.ResponseStatusException;
import com.tit.core.rs.exception.UnprocessableEntityException;
import com.tit.core.rs.helper.ResponseBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

/**
 * Created by danil on 17.09.2016.
 */
@ControllerAdvice
class GlobalControllerExceptionHandler {

    @Autowired
    private LocalizationService localizationService;

    @ResponseStatus(HttpStatus.EXPECTATION_FAILED)
    @ExceptionHandler({
            MethodArgumentNotValidException.class,
            ResponseStatusException.class
    })
    public ResponseEntity handleException(HttpServletRequest req, Exception e) {
        if (e instanceof MethodArgumentNotValidException) {
            ResponseBuilder responseBuilder = ResponseBuilder.status(HttpStatus.UNPROCESSABLE_ENTITY);
            List<FieldError> fieldErrors = ((MethodArgumentNotValidException) e).getBindingResult().getFieldErrors();
            for (FieldError error : fieldErrors) {
                responseBuilder.error(
                        error.getField(),
                        localizationService.translate(error));
            }
            return responseBuilder.get();
        } else if (e instanceof ResponseStatusException) {
            ResponseBuilder responseBuilder = ResponseBuilder.status(HttpStatus.UNPROCESSABLE_ENTITY);
            return responseBuilder
                    .error(localizationService.translate(
                            e.getMessage(),
                            ((UnprocessableEntityException) e).getParams()))
                    .get();
        }
        return null;
    }

}
