package com.tit.core.rs.form;

import org.springframework.http.HttpStatus;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * Created by danil on 17.09.2016.
 */
public class ResponseForm<T> {
    private int status = HttpStatus.OK.value();
    private List<T> rows = new ArrayList<>();
    private Set<ErrorForm> errors = new HashSet<>();

    public ResponseForm(HttpStatus status) {
        this.status = status.value();
    }

    public Set<ErrorForm> getErrors() {
        return errors;
    }

    public int getStatus() {
        return status;
    }

    public List<T> getRows() {
        return rows;
    }
}

