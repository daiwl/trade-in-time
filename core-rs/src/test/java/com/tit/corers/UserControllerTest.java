package com.tit.corers;

import com.tit.core.exception.LoginExistsException;
import com.tit.core.record.UserRecord;
import com.tit.core.service.UserService;
import com.tit.corers.builder.UserTestBuilder;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;

import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.is;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.when;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestBuilders.formLogin;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestBuilders.logout;
import static org.springframework.security.test.web.servlet.response.SecurityMockMvcResultMatchers.authenticated;
import static org.springframework.security.test.web.servlet.response.SecurityMockMvcResultMatchers.unauthenticated;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Created by danil on 11.09.2016.
 */

public class UserControllerTest extends WebGenericTest {

    @Autowired
    protected UserService userService;

    @Test
    public void login() throws Exception {
        mockMvc.perform(formLogin("/auth/login")
                .user("test").password("test")
        )
                .andExpect(status().isFound())
                .andExpect(authenticated().withUsername("test").withRoles("USER"));
    }

    @Test
    public void logoutUser() throws Exception {
        mockMvc.perform(logout("/auth/logout"))
                .andExpect(status().isOk())
                .andExpect(unauthenticated());
    }

    @Test
    public void registration() throws LoginExistsException {
        UserRecord userRecord = UserTestBuilder.testRecord();
        checkFieldErrors(userRecord, 0);

        when(userService.update(any(UserRecord.class)))
                .thenReturn(UserTestBuilder.toEntity(userRecord));

        try {
            this.mockMvc.perform(post("/user/registry")
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(toJson(userRecord))
            )
                    .andExpect(status().isCreated())
                    .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
                    .andExpect(jsonPath("$.rows", hasSize(1)))
                    .andExpect(jsonPath("$.rows[0].id", is(0)));
        } catch (Exception e) {
            assert false;
        }
    }

    @Test
    @WithMockUser(username = "test")
    public void loginExists() throws LoginExistsException {
        UserRecord userRecord = UserTestBuilder.testRecord();

        when(userService.update(any(UserRecord.class)))
                .thenThrow(new LoginExistsException("one"));

        try {
            this.mockMvc.perform(post("/user/registry")
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(toJson(userRecord))
            )
                    .andExpect(status().isUnprocessableEntity())
                    .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
                    .andExpect(jsonPath("$.errors", hasSize(1)));
        } catch (Exception e) {
            assert false;
        }
    }
}
