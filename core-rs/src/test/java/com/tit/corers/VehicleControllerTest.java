package com.tit.corers;

import com.tit.core.record.VehicleRecord;
import org.junit.Test;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

/**
 * Created by danil on 04.09.2016.
 */

public class VehicleControllerTest extends WebGenericTest {

//    @Test
//    public void index() throws Exception {
//        this.mockMvc.perform(get("/"))
//                .andExpect(status().isOk())
//                .andExpect(forwardedUrl("/WEB-INF/views/index.jsp"));
//    }

    @Test
    @WithMockUser(username = "test", password = "test")
    public void update() throws Exception {
        this.mockMvc.perform(post("/vehicle/")
                .contentType(MediaType.APPLICATION_JSON)
                .content(toJson(new VehicleRecord()))
        )
                .andExpect(status().isOk());

//        http://docs.spring.io/spring-security/site/docs/current/reference/html/test-mockmvc.html

//        when(sampleService.saveFrom(any(SignupForm.class)))
//                .thenThrow(new InvalidUserException("For Testing"));

//        mockMvc.perform(post("/hotels/{id}", 42).accept(MediaType.APPLICATION_JSON));
//        assertThat(formObject.getDescription(), is("description"));
//        assertNull(formObject.getId());
//        assertThat(formObject.getTitle(), is("title"));
    }
}
