package com.tit.corers;

import com.tit.core.session.AuthUser;
import com.tit.core.service.LocalizationService;
import com.tit.core.service.UserService;
import com.tit.core.service.VehicleService;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.core.authority.SimpleGrantedAuthority;

import java.util.Collections;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

/**
 * Created by danil on 11.09.2016.
 */
@Configuration
public class WebConfigTest {

    @Bean
    public UserService userService() {
        UserService mock = mock(UserService.class);
        when(mock.loadUserByUsername("test"))
                .thenReturn(new AuthUser(
                        "test",
                        "$2a$10$r33H9YHlRhqJuWnLkj1NB.Cm80Fk1OF741aU1jSL8wM1NU2yA99S2",
                        Collections.singleton(new SimpleGrantedAuthority("ROLE_USER"))));

        return mock;
    }

    @Bean
    public VehicleService vehicleService() {
        return mock(VehicleService.class);
    }

    @Bean
    public LocalizationService localizationService() {
        return mock(LocalizationService.class);
    }

}
