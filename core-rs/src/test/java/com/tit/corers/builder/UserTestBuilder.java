package com.tit.corers.builder;

import com.tit.core.record.UserRecord;
import com.tit.persistence.entity.User;
import org.springframework.test.web.servlet.request.RequestPostProcessor;

import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.user;

/**
 * Created by danil on 17.09.2016.
 */
public class UserTestBuilder {

    public static UserRecord testRecord() {
        UserRecord record = new UserRecord();
        record.setLogin("test");
        record.setPassword("test");
        return record;
    }

    public static User toEntity(UserRecord record) {
        User entity = new User();
        entity.setId(0L);
        entity.setLogin(record.getLogin());
        entity.setPassword(record.getPassword());
        return entity;
    }

    public static RequestPostProcessor testUser() {
        return user("test").roles("USER");
    }
}
