package com.tit.core.record;

import javax.validation.constraints.Size;

/**
 * Created by danil on 03.09.2016.
 */
public class VehicleRecord {

    @Size(min = 3, message = "Длина фамилии должна быть больше трех")
    private String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
