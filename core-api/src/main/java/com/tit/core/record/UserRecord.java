package com.tit.core.record;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 * Created by danil on 11.09.2016.
 */
public class UserRecord {

    @NotNull
    @Size(min = 3, max = 30, message = "{my.test}")
    private String login;

    @NotNull
    @Size(min = 3, max = 30)
    private String password;

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
