package com.tit.core.service;

import com.tit.core.exception.LoginExistsException;
import com.tit.core.record.UserRecord;
import com.tit.persistence.entity.User;
import org.springframework.security.core.userdetails.UserDetailsService;

/**
 * Created by danil on 06.09.2016.
 */
public interface UserService extends UserDetailsService {
    User update(UserRecord record) throws LoginExistsException;
}
