package com.tit.core.service;


import com.tit.core.record.VehicleRecord;
import com.tit.persistence.entity.Vehicle;

/**
 * Created by danil on 03.09.2016.
 */

public interface VehicleService {
    Vehicle update(VehicleRecord record);

    void check();
}
