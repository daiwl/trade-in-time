package com.tit.core.service;

import org.springframework.context.MessageSourceResolvable;

/**
 * Created by danil on 20.09.2016.
 */
public interface LocalizationService {
    String translate(String key, Object... params);

    String translate(MessageSourceResolvable key);
}
