package com.tit.core.validation.validator;

import com.tit.core.record.UserRecord;
import com.tit.core.validation.PasswordMatches;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

/**
 * Created by danil on 11.09.2016.
 */
public class PasswordMatchesValidator implements ConstraintValidator<PasswordMatches, Object> {

    @Override
    public void initialize(PasswordMatches constraintAnnotation) {
    }

    @Override
    public boolean isValid(Object obj, ConstraintValidatorContext context) {
        UserRecord user = (UserRecord) obj;
        return user.getPassword().equals(user.getPassword());
    }
}
