package com.tit.core.exception;

/**
 * Created by danil on 18.09.2016.
 */
public class LoginExistsException extends Exception {
    private String login;

    public LoginExistsException(String login) {
        super();
        this.login = login;
    }

    public String getLogin() {
        return login;
    }
}
