package com.tit.persistence.dao;

import com.tit.persistence.dao.GenericDaoImpl;
import org.springframework.stereotype.Repository;

/**
 * Created by danil on 04.09.2016.
 */

@Repository(value = "vehicleDao")
public class VehicleDaoImpl extends GenericDaoImpl implements VehicleDao {
}
