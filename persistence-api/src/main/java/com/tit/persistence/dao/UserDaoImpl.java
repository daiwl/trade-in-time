package com.tit.persistence.dao;

import com.tit.persistence.dao.GenericDaoImpl;
import com.tit.persistence.entity.User;
import org.springframework.stereotype.Repository;

import javax.persistence.NoResultException;

/**
 * Created by danil on 09.09.2016.
 */

@Repository
public class UserDaoImpl extends GenericDaoImpl implements UserDao {

    @Override
    public User findByLogin(String login) {
        try {
            return (User) getEm().createQuery("select u from User u where u.login = :login")
                    .setParameter("login", login)
                    .getSingleResult();
        } catch (NoResultException e) {
            return null;
        }
    }

//    public void fdf() {
//        Criteria criteria = session.createCriteria(Users.class);
//        criteria.add(Restrictions.eq("Id", Id));
//        criteria.setProjection(Projections.rowCount());
//        long count = (Long) criteria.uniqueResult();
//    }

    @Override
    public boolean isUserWithLogin(String login) {
        return (long) getEm().createQuery("select count(u.id) from User u where u.login = :login")
                .setParameter("login", login)
                .getSingleResult() > 0;
    }
}
