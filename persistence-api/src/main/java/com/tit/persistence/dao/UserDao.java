package com.tit.persistence.dao;


import com.tit.persistence.dao.GenericDao;
import com.tit.persistence.entity.User;

/**
 * Created by danil on 09.09.2016.
 */
public interface UserDao extends GenericDao {
    User findByLogin(String login);

    boolean isUserWithLogin(String login);
}
