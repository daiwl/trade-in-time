import {Component, Input} from '@angular/core';
import {Vehicle} from "../../component/vcard/vcard";

@Component({
    selector: 'auto',
    templateUrl: './auto.template.html'
})

export class AutoComponent {
    vehicles: Vehicle[] = [{
        mark: "Лада",
        cost: 1236456465464,
        year: 2000,
        mileage: 502489658,
        model: "2110",
    }, {
        mark: "Лада",
        cost: 1236456465464,
        year: 2000,
        mileage: 502489658,
        model: "2110",
    }, {
        mark: "Лада",
        cost: 1236456465464,
        year: 2000,
        mileage: 502489658,
        model: "2110",
    }];
}
