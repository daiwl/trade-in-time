"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var AutoComponent = (function () {
    function AutoComponent() {
        this.vehicles = [{
                mark: "Лада",
                cost: 1236456465464,
                year: 2000,
                mileage: 502489658,
                model: "2110",
            }, {
                mark: "Лада",
                cost: 1236456465464,
                year: 2000,
                mileage: 502489658,
                model: "2110",
            }, {
                mark: "Лада",
                cost: 1236456465464,
                year: 2000,
                mileage: 502489658,
                model: "2110",
            }];
    }
    AutoComponent = __decorate([
        core_1.Component({
            selector: 'auto',
            templateUrl: './auto.template.html'
        }), 
        __metadata('design:paramtypes', [])
    ], AutoComponent);
    return AutoComponent;
}());
exports.AutoComponent = AutoComponent;
//# sourceMappingURL=auto.component.js.map