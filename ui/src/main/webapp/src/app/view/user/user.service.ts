import {Injectable} from '@angular/core';
import {Http} from "@angular/http";
import {User} from "./user";
import {Response} from "../../common/response";
import {AbstractService} from "../../common/abstract.service";
import 'rxjs/add/operator/toPromise';

@Injectable()
export class UserService extends AbstractService {
    constructor(public http: Http) {
        super(http);
    }

    registry(user: User): Promise<Response> {
        return this.postJson("/user/registry", user);
    }
}
