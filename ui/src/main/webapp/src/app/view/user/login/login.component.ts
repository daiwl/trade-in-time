import {Component} from '@angular/core';
import {FormBuilder, Validators} from '@angular/forms';
// import { Overlay } from 'angular2-modal';
// import { Modal, BootstrapModalModule } from 'angular2-modal/plugins/bootstrap';


import './login.style.less'
import {User} from "../user";
import {UserService} from "../user.service";
import {FormValidation} from "../../../common/validation";
import {Response} from "../../../common/response";
import {AuthService} from "../../../common/auth.service";

@Component({
    selector: 'login-form',
    templateUrl: './login.template.html'
    // providers: BootstrapModalModule.getProviders(),
    // encapsulation: ViewEncapsulation.None
})

export class LoginComponent extends FormValidation<User> {

    constructor(public fb: FormBuilder, public authService: AuthService) {
        super(new User(), authService);
    }

    login() {
        super.onSubmit();
        if (this.submitted) {
            let comp = this;
            this.authService.login(this.record).then(res => {
                comp.onResponse(res);
            });
        }
    }

    buildForm(): void {
        this.form = this.fb.group({
            username: [this.record.username, [
                Validators.required,
                Validators.minLength(4),
                Validators.maxLength(24)
            ]
            ],
            password: [this.record.password, Validators.required]
        });
        super.buildForm();
    }

}
