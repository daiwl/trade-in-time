import {ValidationMessages} from "../../common/validation";

export class User {

    @ValidationMessages(3, 20)
    username: string;

    @ValidationMessages(3, 20)
    password: string;

    token: string;
}