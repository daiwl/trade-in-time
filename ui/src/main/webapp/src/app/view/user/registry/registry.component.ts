import {Component} from '@angular/core';
import {FormBuilder, Validators, FormControl} from '@angular/forms';

import './registry.style.less'
import {User} from "../user";
import {FormValidation} from "../../../common/validation";
import {UserService} from "../user.service";
import {AuthService} from "../../../common/auth.service";

@Component({
    selector: 'registry-form',
    templateUrl: './registry.template.html'
})

export class RegistryComponent extends FormValidation<User> {

    constructor(public fb: FormBuilder, private userService: UserService, public authService: AuthService) {
        super(new User(), authService);
    }

    registry() {
        super.onSubmit();
        if (this.submitted) {
            this.userService.registry(this.record).then(response => {
                console.log(response)
            });
        }
    }

    buildForm(): void {
        this.form = this.fb.group({
            username: new FormControl(this.record.username,
                [
                    Validators.required,
                    Validators.minLength(4),
                    Validators.maxLength(24)
                ]
            ),
            password: new FormControl(this.record.password,
                [
                    Validators.required,
                    Validators.minLength(4),
                    Validators.maxLength(24)
                ]
            )
        });
        super.buildForm();
    }
}
