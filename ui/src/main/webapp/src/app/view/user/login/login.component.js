"use strict";
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var forms_1 = require('@angular/forms');
// import { Overlay } from 'angular2-modal';
// import { Modal, BootstrapModalModule } from 'angular2-modal/plugins/bootstrap';
require('./login.style.less');
var user_1 = require("../user");
var validation_1 = require("../../../common/validation");
var auth_service_1 = require("../../../common/auth.service");
var LoginComponent = (function (_super) {
    __extends(LoginComponent, _super);
    function LoginComponent(fb, authService) {
        _super.call(this, new user_1.User(), authService);
        this.fb = fb;
        this.authService = authService;
    }
    LoginComponent.prototype.login = function () {
        _super.prototype.onSubmit.call(this);
        if (this.submitted) {
            var comp_1 = this;
            this.authService.login(this.record).then(function (res) {
                comp_1.onResponse(res);
            });
        }
    };
    LoginComponent.prototype.buildForm = function () {
        this.form = this.fb.group({
            username: [this.record.username, [
                    forms_1.Validators.required,
                    forms_1.Validators.minLength(4),
                    forms_1.Validators.maxLength(24)
                ]
            ],
            password: [this.record.password, forms_1.Validators.required]
        });
        _super.prototype.buildForm.call(this);
    };
    LoginComponent = __decorate([
        core_1.Component({
            selector: 'login-form',
            templateUrl: './login.template.html'
        }), 
        __metadata('design:paramtypes', [forms_1.FormBuilder, auth_service_1.AuthService])
    ], LoginComponent);
    return LoginComponent;
}(validation_1.FormValidation));
exports.LoginComponent = LoginComponent;
//# sourceMappingURL=login.component.js.map