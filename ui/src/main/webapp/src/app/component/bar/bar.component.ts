import {Component, Input} from '@angular/core';

import './bar.style.less';
import {AuthService} from "../../common/auth.service";

@Component({
    selector: 'bar',
    templateUrl: './bar.template.html'
})

export class BarComponent {

 constructor(public authService: AuthService) {

 }
}
