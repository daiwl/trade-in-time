import {Component, Input} from '@angular/core';
import {Vehicle} from './vcard';

import './vcard.style.less';

@Component({
    selector: 'v-card',
    templateUrl:'./vcard.template.html'
})

export class VcardComponent {
    @Input()
    vehicle: Vehicle;
}
