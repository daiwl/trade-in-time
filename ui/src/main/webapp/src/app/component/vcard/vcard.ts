export class Vehicle {
    mark: string;
    model: string;
    cost: number;
    year: number;
    mileage: number;
}
