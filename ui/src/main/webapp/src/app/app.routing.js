"use strict";
var router_1 = require('@angular/router');
var login_component_1 = require("./view/user/login/login.component");
var registry_component_1 = require("./view/user/registry/registry.component");
var auto_component_1 = require("./view/auto/auto.component");
var appRoutes = [
    {
        path: '',
        children: [
            {
                path: '',
                redirectTo: 'auto',
            },
            {
                path: 'sign-in',
                component: login_component_1.LoginComponent
            },
            {
                path: 'registry',
                component: registry_component_1.RegistryComponent
            },
            {
                path: 'auto',
                component: auto_component_1.AutoComponent
            },
        ]
    },
    {
        path: '**',
        redirectTo: 'auto'
    }
];
exports.appRouting = router_1.RouterModule.forRoot(appRoutes);
//# sourceMappingURL=app.routing.js.map