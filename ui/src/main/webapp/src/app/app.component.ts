import {Component, OnInit, ViewEncapsulation} from '@angular/core';

// import { ModalModule } from 'angular2-modal';
// import { BootstrapModalModule } from 'angular2-modal/plugins/bootstrap';

import '../stylesheet/global.less';
import './app.style.less';
import {AuthService} from "./common/auth.service";

@Component({
    moduleId: module.id,
    encapsulation: ViewEncapsulation.None,
    selector: 'app',
    templateUrl: './app.template.html'
})
export class AppComponent implements OnInit {
    constructor(public authService: AuthService) {

    }

    ngOnInit(): void {
    }
}
