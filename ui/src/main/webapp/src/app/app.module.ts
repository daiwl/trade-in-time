import {NgModule}               from '@angular/core';
import {FormsModule}            from "@angular/forms";
import {BrowserModule}          from '@angular/platform-browser';
import {ReactiveFormsModule}    from '@angular/forms';

// import { ModalModule } from 'angular2-modal';
// import { BootstrapModalModule } from 'angular2-modal/plugins/bootstrap';

import {AppComponent}   from "./app.component";
import {UserService} from "./view/user/user.service";
import {AutoComponent} from "./view/auto/auto.component";
import {RegistryComponent} from "./view/user/registry/registry.component";
import {BarComponent} from "./component/bar/bar.component";
import {VcardComponent} from "./component/vcard/vcard.component";
import {LoginComponent} from "./view/user/login/login.component";
import {HttpModule} from "@angular/http";
import {appRouting} from './app.routing';
import {LocationStrategy, APP_BASE_HREF, HashLocationStrategy} from "@angular/common";
import {AuthService} from "./common/auth.service";

@NgModule({
    imports: [
        BrowserModule,
        FormsModule,
        ReactiveFormsModule,
        appRouting,
        HttpModule,
        // ModalModule.forRoot(),
        // BootstrapModalModule
    ],
    declarations: [
        AppComponent,
        LoginComponent,
        VcardComponent,
        BarComponent,
        RegistryComponent,
        AutoComponent
    ],
    providers: [
        AuthService,
        UserService,
        { provide: APP_BASE_HREF, useValue: '/'},
        { provide: LocationStrategy, useClass: HashLocationStrategy}
    ],
    bootstrap: [AppComponent]
})

export class AppModule {
}