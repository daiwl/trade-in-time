import { ModuleWithProviders }  from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {LoginComponent} from "./view/user/login/login.component";
import {RegistryComponent} from "./view/user/registry/registry.component";
import {AutoComponent} from "./view/auto/auto.component";

const appRoutes: Routes = [
    {
        path: '',
        children: [
            {
                path: '',
                redirectTo: 'auto',
            },
            {
                path: 'sign-in',
                component: LoginComponent
            },
            {
                path: 'registry',
                component: RegistryComponent
            },
            {
                path: 'auto',
                component: AutoComponent
            },
        ]
    },
    {
        path: '**',
        redirectTo:'auto'
    }
    // {
    //     path: 'heroes',
    //     component: HeroesComponent
    // }
    // {
    //     // path: '/',
    //     // component: AppComponent,
    //     children: [
    //         { path: 'sign-in', component: LoginComponent },
    //         // {
    //         //     path: '',
    //         //     children: [
    //         //         { path: 'sign-in', component: LoginComponent },
    //         //         { path: 'heroes', component: ManageHeroesComponent },
    //         //         { path: '', component: AdminDashboardComponent }
    //         //     ]
    //         // }
    //     ]
    // }
    // { path: 'hero/:id', component: HeroDetailComponent },
    // { path: 'sign-in', component: LoginComponent },
    // {
    //     path: 'heroes',
    //     component: HeroListComponent,
    //     data: {
    //         title: 'Heroes List'
    //     }
    // },
    // { path: '', component: LoginComponent },
    // { path: '**', component: PageNotFoundComponent }
];

export const appRouting: ModuleWithProviders = RouterModule.forRoot(appRoutes);
