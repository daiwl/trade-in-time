/**
 * Created by danil on 09.10.2016.
 */
"use strict";
var Response = (function () {
    function Response(body, error) {
        this.body = body;
        this.error = error;
    }
    return Response;
}());
exports.Response = Response;
//# sourceMappingURL=response.js.map