"use strict";
var http_1 = require("@angular/http");
require('rxjs/add/operator/toPromise');
var AbstractService = (function () {
    function AbstractService(http) {
        this.http = http;
        this.host = "http://localhost:8080/web-api";
        this.headers = new http_1.Headers({ 'Content-Type': 'application/json' });
    }
    AbstractService.handleError = function (error) {
        console.error('An error occurred', error);
        return Promise.reject(error.message || error);
    };
    AbstractService.toForm = function (obj) {
        return Object.keys(obj).map(function (key) {
            return key + '=' + obj[key];
        }).join('&');
    };
    AbstractService.prototype.postJson = function (path, data) {
        this.headers.set('Content-Type', 'application/json');
        return this.post(path, JSON.stringify(data));
    };
    AbstractService.prototype.postForm = function (path, data) {
        this.headers.set('Content-Type', 'application/x-www-form-urlencoded');
        return this.post(path, AbstractService.toForm(data));
    };
    AbstractService.prototype.post = function (path, data) {
        var options = new http_1.RequestOptions({ headers: this.headers });
        var url = "" + this.host + path;
        return this.http.post(url, data, options)
            .toPromise()
            .then(function (response) {
            console.log(response.json());
            if (!response)
                return null;
            return response.json().data;
        })
            .catch(AbstractService.handleError);
    };
    return AbstractService;
}());
exports.AbstractService = AbstractService;
//# sourceMappingURL=abstract.service.js.map