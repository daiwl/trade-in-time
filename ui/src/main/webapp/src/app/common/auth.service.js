"use strict";
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var router_1 = require('@angular/router');
var abstract_service_1 = require("./abstract.service");
var http_1 = require("@angular/http");
var response_1 = require("./response");
var AuthService = (function (_super) {
    __extends(AuthService, _super);
    function AuthService(http, router) {
        _super.call(this, http);
        this.http = http;
        this.router = router;
        this.isLoggedIn = false;
        this.isLoggedIn = !!localStorage.getItem('auth_token');
    }
    AuthService.prototype.login = function (user) {
        var _this = this;
        var headers = new http_1.Headers({ 'Content-Type': 'application/x-www-form-urlencoded' });
        var options = new http_1.RequestOptions({ headers: headers });
        var url = this.host + "/auth/login";
        var service = this;
        return this.http.post(url, abstract_service_1.AbstractService.toForm(user), options)
            .toPromise()
            .then(function (response) {
            console.log(response);
            var user = response.json();
            localStorage.setItem('auth_token', user.token);
            localStorage.setItem('profile', JSON.stringify(user));
            _this.isLoggedIn = true;
            service.router.navigateByUrl('/auto');
            return new response_1.Response(user);
        })
            .catch(function (error) {
            console.log("Error" + error);
            return new response_1.Response(null, "Пользователь с указанными данными не существует");
        });
    };
    AuthService.prototype.logout = function () {
        var _this = this;
        var url = this.host + "/auth/logout";
        var service = this;
        this.http.post(url, {})
            .toPromise()
            .then(function (response) {
            console.log(response);
            localStorage.removeItem('profile');
            localStorage.removeItem('auth_token');
            _this.isLoggedIn = false;
            service.router.navigateByUrl('/auto');
        })
            .catch(function (error) {
            console.log(error);
        });
    };
    AuthService.prototype.loggedIn = function () {
        return this.isLoggedIn;
    };
    AuthService = __decorate([
        core_1.Injectable(), 
        __metadata('design:paramtypes', [http_1.Http, router_1.Router])
    ], AuthService);
    return AuthService;
}(abstract_service_1.AbstractService));
exports.AuthService = AuthService;
//# sourceMappingURL=auth.service.js.map