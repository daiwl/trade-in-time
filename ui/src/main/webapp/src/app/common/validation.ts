import {FormGroup, FormBuilder} from "@angular/forms";
import {OnInit} from "@angular/core";
import {Response} from "./response";
import {AuthService} from "./auth.service";

export function ValidationMessages(min: number, max: number) {
    return function (target: any, propertyKey: string) {
        if (target.constructor.prototype.validationMessages == null) {
            target.constructor.prototype.validationMessages = {};
        }
        if (target.constructor.prototype.validationMessages[propertyKey] == null) {
            target.constructor.prototype.validationMessages[propertyKey] = {
                required: "Поля обязательно для заполнения",
                minlength: "Минимальное значение поля " + min,
                maxlength: "Максимальное значение поля " + max
            };
        }
    };
}

export abstract class FormValidation<T> implements OnInit {
    form: FormGroup;
    submitted = false;
    formErrors = {};
    serverError: string;

    constructor(public record: T, public authService: AuthService) {
    }

    onSubmit() {
        this.onValueChanged();
        if (this.form.invalid) {
            return;
        }
        this.submitted = true;
        this.record = this.form.value;
    }

    ngOnInit(): void {
        this.buildForm();
        this.onValueChanged(this.record)
    }

    onValueChanged(data?: any) {
        if (!this.form) {
            return;
        }
        for (const field in data) {
            if (!data.hasOwnProperty(field)) {
                continue;
            }
            // clear previous error message (if any)
            this.formErrors[field] = '';
            this.serverError = '';
            const control = this.form.get(field);
            if (control && control.dirty && !control.valid) {
                let validationMessages = this.record.constructor.prototype.validationMessages;
                if (validationMessages && validationMessages.hasOwnProperty(field)) {
                    const messages = validationMessages[field];
                    for (const key in control.errors) {
                        this.formErrors[field] += messages[key] + ' ';
                    }
                }
            }
        }
    }

    buildForm(): void {
        this.form.valueChanges.subscribe(data => this.onValueChanged(data));
    }

    onResponse(res: Response) {
        if (res.error) {
            this.serverError = res.error;
        }
        this.submitted = false;
    }
}