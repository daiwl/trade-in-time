import {Injectable} from '@angular/core';
import {Router} from '@angular/router';
import {User} from "../view/user/user";
import {AbstractService} from "./abstract.service";
import {Http, Headers, RequestOptions} from "@angular/http";
import {Response} from "./response";


@Injectable()
export class AuthService extends AbstractService {
    public isLoggedIn = false;

    constructor(public http: Http, private router: Router) {
        super(http);
        this.isLoggedIn = !!localStorage.getItem('auth_token');
    }

    login(user: User): Promise<Response> {
        const headers = new Headers({'Content-Type': 'application/x-www-form-urlencoded'});
        const options = new RequestOptions({headers: headers});
        const url = `${this.host}/auth/login`;
        let service = this;
        return this.http.post(url, AbstractService.toForm(user), options)
            .toPromise()
            .then(response => {
                console.log(response);

                let user = response.json() as User;
                localStorage.setItem('auth_token', user.token);
                localStorage.setItem('profile', JSON.stringify(user));
                this.isLoggedIn = true;
                service.router.navigateByUrl('/auto');
                return new Response(user);
            })
            .catch(function (error) {
                console.log("Error"+ error);
                return new Response(null, "Пользователь с указанными данными не существует");
            });


    }

    logout() {
        const url = `${this.host}/auth/logout`;
        let service = this;
        this.http.post(url, {})
            .toPromise()
            .then(response => {
                console.log(response);

                localStorage.removeItem('profile');
                localStorage.removeItem('auth_token');
                this.isLoggedIn = false;

                service.router.navigateByUrl('/auto');
            })
            .catch(function (error) {
                console.log(error);
            });
    }

    loggedIn() {
        return this.isLoggedIn;
    }
}