import {Http, Headers, RequestOptions} from "@angular/http";
import {Response} from "./response";
import 'rxjs/add/operator/toPromise';

export abstract class AbstractService {
    host:string = "http://localhost:8080/web-api";
    headers: Headers = new Headers({'Content-Type': 'application/json'});

    constructor(public http: Http) {
    }

    static handleError(error: any): Promise<any> {
        console.error('An error occurred', error);
        return Promise.reject(error.message || error);
    }

    static toForm(obj: Object): string {
        return Object.keys(obj).map(function (key) {
            return key + '=' + obj[key];
        }).join('&');
    }

    postJson(path: string, data: Object): Promise<Response> {
        this.headers.set('Content-Type', 'application/json');
        return this.post(path, JSON.stringify(data));
    }

    postForm(path: string, data: Object): Promise<Response> {
        this.headers.set('Content-Type', 'application/x-www-form-urlencoded');
        return this.post(path, AbstractService.toForm(data));
    }

    post(path: string, data: string): Promise<Response> {
        const options = new RequestOptions({headers: this.headers});
        const url = `${this.host}${path}`;
        return this.http.post(url, data, options)
            .toPromise()
            .then(response => {
                console.log(response.json());
                if (!response) return null;
                return response.json().data as Response
            })
            .catch(AbstractService.handleError);
    }
}
