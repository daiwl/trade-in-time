"use strict";
function ValidationMessages(min, max) {
    return function (target, propertyKey) {
        if (target.constructor.prototype.validationMessages == null) {
            target.constructor.prototype.validationMessages = {};
        }
        if (target.constructor.prototype.validationMessages[propertyKey] == null) {
            target.constructor.prototype.validationMessages[propertyKey] = {
                required: "Поля обязательно для заполнения",
                minlength: "Минимальное значение поля " + min,
                maxlength: "Максимальное значение поля " + max
            };
        }
    };
}
exports.ValidationMessages = ValidationMessages;
var FormValidation = (function () {
    function FormValidation(record, authService) {
        this.record = record;
        this.authService = authService;
        this.submitted = false;
        this.formErrors = {};
    }
    FormValidation.prototype.onSubmit = function () {
        this.onValueChanged();
        if (this.form.invalid) {
            return;
        }
        this.submitted = true;
        this.record = this.form.value;
    };
    FormValidation.prototype.ngOnInit = function () {
        this.buildForm();
        this.onValueChanged(this.record);
    };
    FormValidation.prototype.onValueChanged = function (data) {
        if (!this.form) {
            return;
        }
        for (var field in data) {
            if (!data.hasOwnProperty(field)) {
                continue;
            }
            // clear previous error message (if any)
            this.formErrors[field] = '';
            this.serverError = '';
            var control = this.form.get(field);
            if (control && control.dirty && !control.valid) {
                var validationMessages = this.record.constructor.prototype.validationMessages;
                if (validationMessages && validationMessages.hasOwnProperty(field)) {
                    var messages = validationMessages[field];
                    for (var key in control.errors) {
                        this.formErrors[field] += messages[key] + ' ';
                    }
                }
            }
        }
    };
    FormValidation.prototype.buildForm = function () {
        var _this = this;
        this.form.valueChanges.subscribe(function (data) { return _this.onValueChanged(data); });
    };
    FormValidation.prototype.onResponse = function (res) {
        if (res.error) {
            this.serverError = res.error;
        }
        this.submitted = false;
    };
    return FormValidation;
}());
exports.FormValidation = FormValidation;
//# sourceMappingURL=validation.js.map