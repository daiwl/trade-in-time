"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var forms_1 = require("@angular/forms");
var platform_browser_1 = require('@angular/platform-browser');
var forms_2 = require('@angular/forms');
// import { ModalModule } from 'angular2-modal';
// import { BootstrapModalModule } from 'angular2-modal/plugins/bootstrap';
var app_component_1 = require("./app.component");
var user_service_1 = require("./view/user/user.service");
var auto_component_1 = require("./view/auto/auto.component");
var registry_component_1 = require("./view/user/registry/registry.component");
var bar_component_1 = require("./component/bar/bar.component");
var vcard_component_1 = require("./component/vcard/vcard.component");
var login_component_1 = require("./view/user/login/login.component");
var http_1 = require("@angular/http");
var app_routing_1 = require('./app.routing');
var common_1 = require("@angular/common");
var auth_service_1 = require("./common/auth.service");
var AppModule = (function () {
    function AppModule() {
    }
    AppModule = __decorate([
        core_1.NgModule({
            imports: [
                platform_browser_1.BrowserModule,
                forms_1.FormsModule,
                forms_2.ReactiveFormsModule,
                app_routing_1.appRouting,
                http_1.HttpModule,
            ],
            declarations: [
                app_component_1.AppComponent,
                login_component_1.LoginComponent,
                vcard_component_1.VcardComponent,
                bar_component_1.BarComponent,
                registry_component_1.RegistryComponent,
                auto_component_1.AutoComponent
            ],
            providers: [
                auth_service_1.AuthService,
                user_service_1.UserService,
                { provide: common_1.APP_BASE_HREF, useValue: '/' },
                { provide: common_1.LocationStrategy, useClass: common_1.HashLocationStrategy }
            ],
            bootstrap: [app_component_1.AppComponent]
        }), 
        __metadata('design:paramtypes', [])
    ], AppModule);
    return AppModule;
}());
exports.AppModule = AppModule;
//# sourceMappingURL=app.module.js.map