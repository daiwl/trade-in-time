import {platformBrowserDynamic} from '@angular/platform-browser-dynamic';
import {AppModule} from './app/app.module';
import {enableProdMode} from "@angular/core";

// import {getTranslationProviders} from "./i18n-providers";

if (process.env.ENV === 'production') {
    console.log("************Production***********");
    enableProdMode();
}
//
// getTranslationProviders().then(providers => {
//     const options = {providers};
//     platformBrowserDynamic()
//         .bootstrapModule(AppModule, options)
//         .catch(err => console.error(err));
// });
//
platformBrowserDynamic()
    .bootstrapModule(AppModule)
    .catch(err => console.error(err));