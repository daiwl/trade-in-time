"use strict";
var platform_browser_dynamic_1 = require('@angular/platform-browser-dynamic');
var app_module_1 = require('./app/app.module');
var core_1 = require("@angular/core");
// import {getTranslationProviders} from "./i18n-providers";
if (process.env.ENV === 'production') {
    console.log("************Production***********");
    core_1.enableProdMode();
}
//
// getTranslationProviders().then(providers => {
//     const options = {providers};
//     platformBrowserDynamic()
//         .bootstrapModule(AppModule, options)
//         .catch(err => console.error(err));
// });
//
platform_browser_dynamic_1.platformBrowserDynamic()
    .bootstrapModule(app_module_1.AppModule)
    .catch(function (err) { return console.error(err); });
//# sourceMappingURL=main.js.map