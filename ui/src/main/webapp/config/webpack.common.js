const webpack = require('webpack');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const ExtractTextPlugin = require('extract-text-webpack-plugin');
const helpers = require('./helpers');

const CopyWebpackPlugin = require('copy-webpack-plugin');
const ContextReplacementPlugin = require('webpack/lib/ContextReplacementPlugin');

module.exports = function (options) {

    return {
        entry: {
            'polyfills': helpers.root('src/polyfills.ts'),
            'vendor': helpers.root('src/vendor.ts'),
            'app': helpers.root('src/main.ts')
        },

        output: {
            path: helpers.root('dist')
        },

        resolve: {
            extensions: ['.ts', '.js'],
            modules: [helpers.root('src'), 'node_modules']
        },

        module: {
            loaders: [
                {
                    test: /\.ts$/,
                    loaders: ['awesome-typescript-loader', 'angular2-template-loader'],
                    exclude: [/\.(spec|e2e)\.ts$/]
                },
                {
                    test: /\.html$/,
                    loader: 'html',
                    exclude: [helpers.root('src/index.html')]
                },
                {
                    test: /\.(png|jpe?g|gif|svg|woff|woff2|ttf|eot|ico)$/,
                    loader: 'file?name=assets/img/[name].[hash].[ext]'
                },
                {
                    test: /\.less$/,
                    loader: ExtractTextPlugin.extract(
                        {fallbackLoader: 'style', loader: 'css?sourceMap!postcss!less' })
                },
                // {
                //     test: /\.less$/,
                //     include: helpers.root('src', 'app'),
                //     loader: commonCSS.extract("style", "css!postcss!less")
                // },
                // {
                //     test: /bootstrap\.less$/,
                //     include: helpers.root('src', 'stylesheet'),
                //     loader: bootstrapCSS.extract('style', 'css!postcss!less')
                // }
                // {
                //     test: /\.css$/,
                //     include: [helpers.root('node_modules/bootstrap/dist/css/')],
                //     loader: bootstrapCSS.extract('style', 'css?sourceMap')
                // }
                // {
                //     test: /.css$/,
                //     exclude: helpers.root('src'),
                //     include: /(node_modules\/bootstrap\/dist\/css\/)/,
                //     loader: bootstrapCSS.extract('style', 'css?sourceMap')
                // }
                // {
                //     test: /\.less$/,
                //     include: helpers.root('src', 'app'),
                //     // loaders: "raw-loader!postcss-loader!less-loader"
                //     loaders: ['to-string-loader', 'css-loader', 'postcss-loader', 'less-loader']
                // },
                // {
                //     test: /\.less$/,
                //     loader: ExtractTextPlugin.extract("style-loader", "css-loader!postcss-loader!less-loader")
                // }
            ]
        },

        // htmlLoader: {
        //     minimize: true,
        //     removeAttributeQuotes: false,
        //     caseSensitive: true,
        //     customAttrSurround: [[/#/, /(?:)/], [/\*/, /(?:)/], [/\[?\(?/, /(?:)/]],
        //     customAttrAssign: [/\)?\]?=/]
        // },

        plugins: [
            new ContextReplacementPlugin(
                /angular(\\|\/)core(\\|\/)(esm(\\|\/)src|src)(\\|\/)linker/,
                helpers.root('src') // location of your src
            ),

            new webpack.optimize.CommonsChunkPlugin({
                name: ['app', 'vendor', 'polyfills']
            }),

            new CopyWebpackPlugin([{
                from: 'src/assets',
                to: 'assets'
            }]),

            new HtmlWebpackPlugin({
                title: "Beep Peep",
                inject: true,
                template: 'src/index.html',
                chunksSortMode: 'dependency'
            }),
        ],

        node: {
            global: true,
            crypto: 'empty',
            process: true,
            module: false,
            clearImmediate: false,
            setImmediate: false
        }
    }
};

